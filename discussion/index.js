console.log("Objects Discussion");
console.log("");
/* 
    Objects
    - an object is a dataype that is used to 
    represent real world objects, also it is a 
    collection of related data and/or funtionalities.
*/

/* Creating Objects using object literal:

    Syntax: 
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }


*/

let student = {
    firstName: "Emman",
    lastName: "Garcia",
    Age: "24",
    StudentId: "2022-009762",
    email: ["ems@gmail.com", "efdgEgmail.com"],
    address: {
        street: "#1 Metro Street",
        city: "Pampanga",
        country: "Philippines"
    }

}

console.log("Result of Creating an Object: ");
console.log(student)
console.log(typeof student);

console.log("");

//Creating Objects using Constructor Function

/* 
        Create a reusable function to create several objects that
        have the same data structure. This useful for creating multiple instances/copies of an object.

        Syntax:
        function objectName(valueA, valueB){
            this.keyA = valueA
            this.keyB = valueB

        }
            let variable = new function ObjectName(valueA,valueB);
            console.log(variable)

            "this" is a keyword used for invoking; in refers to 
            global object
            -dont forget to add "new" keyword when creating the variables

*/
function Laptop(name, manufactureDate) {
    this.name = name
    this.manufactureDate = manufactureDate
}
let laptop = new Laptop("Lenovo", 2008);
console.log("🚀 Result of creating object using object constructor ")
console.log(laptop);

let mylaptop = new Laptop("MacBook", 2021);
console.log("🚀 Result of creating object using object constructor ")
console.log(mylaptop);


let oldLaptop = Laptop("Portal R2E CMC", 1980);
console.log("🚀 Result of creating object using object constructor ")
console.log(oldLaptop);
console.log("");
console.log("Empty Objects");
console.log("");

let computer = {}

let myComputer = new Object()

console.log("🚀 ~ file: index.js ~ line 82 ~ computer", computer)
console.log("🚀 ~ file: index.js ~ line 84 ~ myComputer", myComputer)

myComputer = {

    name: "Asus",
    manufactureDate: 2012
}
console.log(myComputer);

//Mini Activity


function Food(nameFood, tasteFood, colorFood) {
    this.nameFood = nameFood
    this.tasteFood = tasteFood
    this.colorFood = colorFood
}
let bcexpress = new Food("Bicol Express", "Spicy", "Off-White");
console.log("🚀 Food: ")
console.log(bcexpress);

let adobo = new Food("Adobo", "Salty", "Brown");
console.log("🚀 Food: ")
console.log(adobo);


console.log("");
/* 
        Accessing Objective Property


*/

//Using the dot notation
/* Suntax: objectName.propertyName */

console.log("Result form dot notaion : " + mylaptop.name);

// Using the bracket notation
/* Syntax: objectName["name"] */

console.log("Result form bracket notaion : " + mylaptop["name"]);


//Accessing array objects

let array = [laptop, mylaptop];
// let arrray = [{name: "Lenovo", manufactureDate: 2008}, {name:"Macbook",manufactureDate:2019 }]
//dot notation
console.log(array[0].name);
//bracket notation
console.log(array[0]["name"]);

console.log("");


//Initializing/ Adding / Deleting/ Reassinging Object Properties

let car = {}
console.log(car);

//Adding Object properties
car.name = "Honda Civic"
console.log("");
console.log(car);



car["manufacture date"] = 2019
console.log(car);

// car.name = ["Ferrari", "Toyota"]
// console.log(car);
//deleting obj propertires
console.log("");
delete car["manufacture date"]
// car["manufacture date"] = "";
console.log("Result from deleting object preperties:");
console.log(car);

//Reassigning obj properties
car.name = "Tesla"
console.log("Result from reassigning object preperties:");
console.log(car);


console.log("");

// OBJECT METHOD
/* 
    a method where a function serves as a value in a value.
    -they are also functions and one of the key differences they have
    is that methods are functions related to a object property.
*/

let person = {

    name: "John",
    talk: function () {
        console.log("Hello My Name is: " + this.name);
    }
}

console.log("");

console.log(person);
console.log("Result From Object Methods: ");
person.talk();

person.walk = function name(params) {
    console.log(this.name + " walked 25 steps forward");

}
person.walk()

console.log("");

let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

//REAL WORLD APPLICATION

/* 
Scenario:
1. We Would Like to create a game that would have several pokemon to
interact with each other.
2.Every Pokemon would have the same sets of stats, properties and functions.

 */
//Using Object Literal:
// let myPokemon = {
//     name:"Pikachu",
//     level: 3, 
//     health: 100,
//     attack: 50,
//     tackle: function (){
//         console.log("This pokemon tackled targetPokemon");
//         console.log(" target Pokemon's health is now reduced to target PoekmonHealth");
//     },
//     faint: function(){
//         console.log("Pokemon fainted");
//     }
// }
// console.log(myPokemon);

//using obj constructor:
// console.log("");

// function Pokemon(name,level){

// //properties
//     this.name = name;
// this.level = level;
// this.health = 3*level;
// this.attack = 2*level;

// //Methods
// this.tackle = function(target){
//     console.log(this.name + " tackled " + target.name);
//     console.log(target.name + "'s health is now reduced " + 
//     (target.health - this.attack));
// },
// this.faint = function(){
// console.log(this.name + " fainted");
// }


// }

// let charizard = new Pokemon("Charizard" , 12)
// let squirtle = new Pokemon("Squirtle" , 6)

// console.log(charizard);
// console.log(squirtle);
// console.log()

// charizard.tackle(squirtle);


//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.

*/


/*
Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:

//Part1


let poketrainer = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function () {
        console.log("Pikachu! I choose you!");
    },
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    }
};

console.log("🚀 ", poketrainer)
console.log("🚀 Dot notation: ", poketrainer.name)
console.log("🚀 Bracket notation: ", poketrainer['pokemon']);
console.log("🚀 Talk method: ")
console.log(poketrainer.talk());







//Part 2
console.log("");

function Pokemon(name, level) {


    this.name = name
    this.level = level
    this.health = 3 * level
    this.attack = 2 * level


    this.tackle = function (target) {

        console.log("🚀 " + this.name + " tackled " + target.name)
     
        this.faint = function () {
            console.log("🚀 " + target.name + " fainted")
        }

        target.health = (target.health - this.attack);
        if ((target.health - this.attack) <= 5) {
            this.faint();
        }else{
            console.log("🚀 " + target.name + "'s health is now reduced to = " +
            (target.health - this.attack))
        }
    }

}



let ditto = new Pokemon("Ditto", 12)
let entei = new Pokemon("Entei", 19)
let rayquaza = new Pokemon("Rayquaza", 19)

console.log(ditto);
console.log(entei);
console.log("");
ditto.tackle(entei);
console.log("");
rayquaza.tackle(ditto);